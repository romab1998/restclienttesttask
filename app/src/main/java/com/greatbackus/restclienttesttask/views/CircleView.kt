package com.greatbackus.restclienttesttask.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.greatbackus.restclienttesttask.R

/**
 * TODO: document your custom view class.
 */
class CircleView : View {

    private var _circleColor: Int = ContextCompat.getColor(context, R.color.eyesBrown)

    private var paint: Paint = Paint()

    /**
     * The font color
     */
    var circleColor: Int
        get() = _circleColor
        set(value) {
            _circleColor = value
            reconfigurePaint()
        }

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.CircleView, defStyle, 0
        )
        _circleColor = a.getColor(
            R.styleable.CircleView_circleColor,
            circleColor
        )

        a.recycle()

        // Update TextPaint and text measurements from attributes
        reconfigurePaint()
    }

    private fun reconfigurePaint() {
        paint.color = _circleColor
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawCircle(width / 2f, height / 2f, height/2f, paint)

    }
}

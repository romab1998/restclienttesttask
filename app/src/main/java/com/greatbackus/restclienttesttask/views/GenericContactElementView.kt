package com.greatbackus.restclienttesttask.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import com.greatbackus.restclienttesttask.R
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

val formatter = DecimalFormat("#.####", DecimalFormatSymbols.getInstance())

@BindingAdapter("userDetailsText")
fun setText(view: View, text: Any?) {
    val textView = view.findViewById<TextView>(R.id.mainText)
    if (text is Number) {
        textView.text = formatter.format(text)
        return
    }
    if (text != null) {
        textView.text = text.toString().trimEnd()
    }
}


@BindingAdapter("userDetailsHeading")
fun setHeading(view: View, text: String?) {
    if (text != null) view.findViewById<TextView>(R.id.genericContactElement_heading).text = text
}

class GenericContactElementView : ConstraintLayout {

    var ctx: Context
    var attributeSet: AttributeSet? = null

    private fun initialize() {
        val inflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.generic_contact_element, this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        val text = findViewById<TextView>(R.id.mainText)
        val heading = findViewById<TextView>(R.id.genericContactElement_heading)
        val typedArray = context.theme.obtainStyledAttributes(attributeSet, R.styleable.GenericContactElementView, 0, 0).apply {
            text.text = getString(R.styleable.GenericContactElementView_userDetailsText)
            heading.text = getString(R.styleable.GenericContactElementView_userDetailsHeading)
        }
        typedArray.recycle()
    }

    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs) {
        this.ctx = ctx
        this.attributeSet = attrs
        initialize()
    }

    constructor(ctx: Context) : super(ctx) {
        this.ctx = ctx
        initialize()
    }

    constructor(ctx: Context, attrs: AttributeSet, defStyle: Int) : super(ctx, attrs, defStyle) {
        this.ctx = ctx
        this.attributeSet = attrs
        initialize()
    }

}
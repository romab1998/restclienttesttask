package com.greatbackus.restclienttesttask.repositories

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import androidx.lifecycle.LiveData
import com.greatbackus.restclienttesttask.db.UserDao
import com.greatbackus.restclienttesttask.models.FatUserModel
import com.greatbackus.restclienttesttask.resource.DbResource
import com.greatbackus.restclienttesttask.resource.NetworkResource
import com.greatbackus.restclienttesttask.rest.UsersService
import com.greatbackus.restclienttesttask.utils.RateLimiter
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(var usersService: UsersService, var userDao: UserDao, private val connectivityManager: ConnectivityManager, context: Context) {
    private val rateLimiter = RateLimiter(5, TimeUnit.MINUTES, context)

     fun getUsers(force: Boolean=false): LiveData<List<FatUserModel>> {
        return object : NetworkResource<List<FatUserModel>>() {
            override suspend fun fromNetwork(): List<FatUserModel> {
                val tmp = usersService.getUsers().await()
                rateLimiter.fetchSuccess()
                return tmp
            }
            override suspend fun saveResult(item: List<FatUserModel>) {
                userDao.insert(*item.toTypedArray())
            }

            override suspend fun fromDb(): List<FatUserModel> {
                return userDao.getUsers()
            }

            override fun shouldFetch(): Boolean {
                val value = shouldFetchData() || force
                Log.d("shouldFetch", value.toString())
                return value
            }

        }.asLiveData()
    }

    fun getUser(id: Long): LiveData<FatUserModel?> {
        return object : DbResource<FatUserModel?>() {
            override suspend fun updateCachedData() {
                userDao.insert(*usersService.getUsers().await().toTypedArray())
                rateLimiter.fetchSuccess()
            }

            override fun shouldFetch(): Boolean = shouldFetchData()

            override suspend fun fromDb(): FatUserModel? = userDao.getUserById(id)
        }.asLiveData()

    }

    fun getUsers(id: Array<Long>): LiveData<List<FatUserModel>> {
        return object : DbResource<List<FatUserModel>>() {
            override suspend fun updateCachedData() {
                userDao.insert(*usersService.getUsers().await().toTypedArray())
                rateLimiter.fetchSuccess()
            }

            override suspend fun fromDb(): List<FatUserModel>  = userDao.getUsersByIds(id)

            override fun shouldFetch(): Boolean = shouldFetchData()

        }.asLiveData()
    }

    private fun shouldFetchData(): Boolean {
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return this.rateLimiter.shouldFetch() && activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}
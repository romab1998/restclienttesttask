package com.greatbackus.restclienttesttask.viewmodels

import androidx.lifecycle.*
import com.greatbackus.restclienttesttask.models.FatUserModel
import com.greatbackus.restclienttesttask.repositories.UserRepository
import javax.inject.Inject

class UserDetailsViewModel @Inject constructor(val userRepository: UserRepository) : ViewModel() {
    private var _userModelId = MutableLiveData<Long>()
    private var _userModelObserver: Observer<FatUserModel?>

    var userModel = Transformations.switchMap(_userModelId) { input ->
        userRepository.getUser(input)
    }

    var loaded = Transformations.map(userModel) { input: FatUserModel? ->
        input != null
    }

    var friends = MediatorLiveData<List<FatUserModel>>()

    var tags = Transformations.map(userModel) {
        if (it==null) arrayListOf() else it.tags
    }

    init {
        _userModelObserver = Observer {
            if (it != null) {
                friends.addSource(userRepository.getUsers(it.friends.map { it.friendId }.toTypedArray())) {
                    friends.postValue(it)
                }
            }
        }
        userModel.observeForever(_userModelObserver)
    }

    fun setUserId(id: Long) {
        _userModelId.value = id
    }

    override fun onCleared() {
        super.onCleared()
        userModel.removeObserver(_userModelObserver)
    }
}

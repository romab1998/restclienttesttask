package com.greatbackus.restclienttesttask.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.greatbackus.restclienttesttask.models.FatUserModel
import com.greatbackus.restclienttesttask.repositories.UserRepository
import javax.inject.Inject

class UserListViewModel @Inject constructor(private val userRepository: UserRepository) : ViewModel() {
    var usersData: MediatorLiveData<List<FatUserModel>> = MediatorLiveData()
    var dataLoaded: LiveData<Boolean> = Transformations.map(usersData) { input: List<FatUserModel>? ->
        input != null && input.isNotEmpty()
    }

    init {
        loadUsers()
    }

    private fun loadUsers(force: Boolean = false) {
        val users = userRepository.getUsers(force)
        usersData.addSource(users) {
            usersData.value = it
            usersData.removeSource(users)
        }
    }

    fun reload() {
        loadUsers(true)
    }
}
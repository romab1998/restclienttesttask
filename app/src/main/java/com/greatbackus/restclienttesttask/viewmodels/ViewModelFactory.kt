package com.greatbackus.restclienttesttask.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
class ViewModelFactory @Inject constructor(var viewModelsMap: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val creator = viewModelsMap[modelClass] ?:viewModelsMap.entries.firstOrNull{
            modelClass.isAssignableFrom(it.key)
        }?.value ?:throw IllegalArgumentException("Can not instantiate class ${modelClass.name}")
        @Suppress("UNCHECKED_CAST")
        try {
            return creator.get() as T
        } catch (exc: Exception) {
            throw RuntimeException(exc)
        }
    }
}
package com.greatbackus.restclienttesttask.bindings

import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.greatbackus.restclienttesttask.R
import com.greatbackus.restclienttesttask.models.EyeColorEnum
import com.greatbackus.restclienttesttask.views.CircleView


object BindingAdapters {

    @JvmStatic
    @BindingAdapter("eyeColor")
    fun eyeColor(view: View, color: EyeColorEnum?) {
        if (view is CircleView) {
            when (color) {
                EyeColorEnum.BROWN -> view.circleColor = ContextCompat.getColor(view.context, R.color.eyesBrown)
                EyeColorEnum.GREEN -> view.circleColor = ContextCompat.getColor(view.context, R.color.eyesGreen)
                EyeColorEnum.BLUE -> view.circleColor = ContextCompat.getColor(view.context, R.color.eyesBlue)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageResource(imageView: ImageView, resource: Int) {
        imageView.setImageResource(resource)
    }
}
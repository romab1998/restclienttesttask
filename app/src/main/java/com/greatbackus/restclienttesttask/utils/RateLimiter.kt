package com.greatbackus.restclienttesttask.utils

import android.content.Context
import android.content.SharedPreferences
import com.greatbackus.restclienttesttask.R
import java.util.concurrent.TimeUnit

/**
 * Represents a decider for single endpoint, which
 * decides whether to fetch resources or get it from stored db
 *
 * @constructor Creates limiter with specified parameters
 */
class RateLimiter(private val unit: Long, private val timeUnit: TimeUnit, context: Context) {
    val now: Long
        get() = System.currentTimeMillis()
    var lastFetchTime: Long
    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(context.getString(R.string.cacheSharedPreferences), Context.MODE_PRIVATE)
    // save key to not save reference to context or resources or other activity-related stuff
    private val cacheKey = context.getString(R.string.cacheDate)

    init {
        lastFetchTime = sharedPreferences.getLong(cacheKey, 0)
    }

    fun fetchSuccess() {
        lastFetchTime = now
        with (sharedPreferences.edit()) {
            putLong(cacheKey, lastFetchTime)
            apply()
        }
    }

    /**
     * @return Should resource fetch from network or not
     */
    fun shouldFetch(): Boolean {
        return (now - lastFetchTime) > timeUnit.toMillis(unit)
    }

}

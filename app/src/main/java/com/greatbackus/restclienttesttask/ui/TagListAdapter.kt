package com.greatbackus.restclienttesttask.ui

import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.greatbackus.restclienttesttask.models.TagModel


class TagListAdapter(val items: LiveData<List<TagModel>>): ListAdapter<TagModel, TagListAdapter.ViewHolder>(object : DiffUtil.ItemCallback<TagModel>(){
    override fun areItemsTheSame(oldItem: TagModel, newItem: TagModel): Boolean = oldItem.tag == newItem.tag && oldItem.userId == newItem.userId

    override fun areContentsTheSame(oldItem: TagModel, newItem: TagModel): Boolean = oldItem.tag == newItem.tag

}) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(TextView(parent.context))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setValue(items.value!![position].tag)
    }

    class ViewHolder(val view: TextView): RecyclerView.ViewHolder(view) {
        fun setValue(text: String) {
            view.text = "#$text"
        }
    }
}
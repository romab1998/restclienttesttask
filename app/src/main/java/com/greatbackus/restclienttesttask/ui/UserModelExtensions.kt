package com.greatbackus.restclienttesttask.ui

import android.location.Location
import com.greatbackus.restclienttesttask.R
import com.greatbackus.restclienttesttask.models.FavouriteFruitEnum
import com.greatbackus.restclienttesttask.models.UserModel
import java.text.SimpleDateFormat
import java.util.*

fun UserModel?.formattedDate(): String = if (this==null) "" else SimpleDateFormat("HH:mm dd.MM.yy", Locale.getDefault()).format(this.registered)

fun UserModel?.favouriteFruitResource(): Int = if (this==null) R.drawable.apple else when (this.favouriteFruit) {
    FavouriteFruitEnum.BANANA -> R.drawable.banana
    FavouriteFruitEnum.STRAWBERRY -> R.drawable.strawberry
    FavouriteFruitEnum.APPLE -> R.drawable.apple
}

fun UserModel?.formattedCoords(): String = if (this==null) "" else "${Location.convert(Math.abs(this.latitude), Location.FORMAT_SECONDS)} ${if (this.latitude>0) "N" else "S"} ${Location.convert(Math.abs(this.longitude), Location.FORMAT_SECONDS)} ${if (this.longitude>0) "E" else "W"}"
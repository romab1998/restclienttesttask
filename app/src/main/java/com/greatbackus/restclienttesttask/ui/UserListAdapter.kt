package com.greatbackus.restclienttesttask.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.greatbackus.restclienttesttask.databinding.UserListItemBinding
import com.greatbackus.restclienttesttask.models.FatUserModel

class UserListAdapter(var items: LiveData<List<FatUserModel>>, val onClickListener: OnUserListItemListener) : ListAdapter<FatUserModel, UserListAdapter.ViewHolder>(
    object : DiffUtil.ItemCallback<FatUserModel>() {
        override fun areItemsTheSame(oldItem: FatUserModel, newItem: FatUserModel): Boolean {
            return oldItem.userModel.id == newItem.userModel.id
        }

        override fun areContentsTheSame(oldItem: FatUserModel, newItem: FatUserModel): Boolean {
            return oldItem == newItem
        }
    }
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = UserListItemBinding.inflate(layoutInflater, parent, false)
        binding.listener = onClickListener
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (items.value.isNullOrEmpty()) return
        holder.bind(items.value.orEmpty()[position])
        holder.binding.executePendingBindings()
    }

    class ViewHolder(val binding: UserListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(user: FatUserModel) {
            binding.user = user
            binding.executePendingBindings()
        }

    }

}
package com.greatbackus.restclienttesttask.ui

import com.greatbackus.restclienttesttask.models.FatUserModel

interface OnUserListItemListener {
    fun onClick(item: FatUserModel)
}
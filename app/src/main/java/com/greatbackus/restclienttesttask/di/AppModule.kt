package com.greatbackus.restclienttesttask.di

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import androidx.room.Room
import com.greatbackus.restclienttesttask.db.UserDao
import com.greatbackus.restclienttesttask.db.UserDb
import com.greatbackus.restclienttesttask.repositories.UserRepository
import com.greatbackus.restclienttesttask.rest.UsersService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    // no need for migrations since db stores only cache data and this is a test app, for god's sake!
    @Singleton
    @Provides
    fun provideDb(app: Application): UserDb =
        Room
            .databaseBuilder(
                app.applicationContext,
                UserDb::class.java,
                "user.db"
            ).fallbackToDestructiveMigration().build()


    @Singleton
    @Provides
    fun provideUserDao(db: UserDb): UserDao = db.userDao()

    @Singleton
    @Provides
    fun provideUsersService() = UsersService.create()

    @Singleton
    @Provides
    fun provideConnectivityManager(app: Application): ConnectivityManager =
        app.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    @Singleton
    @Provides
    fun provideUserRepository(
        app: Application,
        usersService: UsersService,
        userDao: UserDao,
        connectivityManager: ConnectivityManager
    ) = UserRepository(usersService, userDao, connectivityManager, app.applicationContext)
}
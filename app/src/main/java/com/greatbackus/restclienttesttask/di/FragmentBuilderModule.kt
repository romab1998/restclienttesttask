package com.greatbackus.restclienttesttask.di

import com.greatbackus.restclienttesttask.fragments.UserDetailsFragment
import com.greatbackus.restclienttesttask.fragments.UserModelFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {
    @ContributesAndroidInjector
    abstract fun contributesUserModelFragment(): UserModelFragment

    @ContributesAndroidInjector
    abstract fun contributesUserDetailsFragment(): UserDetailsFragment
}
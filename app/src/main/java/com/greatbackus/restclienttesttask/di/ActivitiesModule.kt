package com.greatbackus.restclienttesttask.di

import com.greatbackus.restclienttesttask.activities.UserDetailsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {
    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contibuteUserDetailsActivity(): UserDetailsActivity
}
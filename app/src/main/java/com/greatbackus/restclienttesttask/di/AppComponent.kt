package com.greatbackus.restclienttesttask.di

import android.app.Application
import com.greatbackus.restclienttesttask.RestClientTestTaskApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, ActivitiesModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        @BindsInstance
        fun application(application: Application): Builder
    }

    fun inject(restClientTestTaskApplication: RestClientTestTaskApplication)
}
package com.greatbackus.restclienttesttask.fragments

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.greatbackus.restclienttesttask.R
import com.greatbackus.restclienttesttask.databinding.FragmentUsermodelListBinding
import com.greatbackus.restclienttesttask.models.FatUserModel
import com.greatbackus.restclienttesttask.ui.OnUserListItemListener
import com.greatbackus.restclienttesttask.ui.UserListAdapter
import com.greatbackus.restclienttesttask.viewmodels.UserListViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class UserModelFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var userListViewModel: UserListViewModel
    lateinit var binding: FragmentUsermodelListBinding
    lateinit var userAdapter: UserListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.user_list_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_reload -> {
                userListViewModel.usersData.value = arrayListOf()
                userListViewModel.reload()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_usermodel_list, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userListViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(UserListViewModel::class.java)

        // Set the adapter
        userAdapter = UserListAdapter(userListViewModel.usersData, object : OnUserListItemListener {
            override fun onClick(item: FatUserModel) {
                findNavController().navigate(
                    UserModelFragmentDirections.actionUserModelFragmentToUserDetailsFragment(
                        item.userModel.id
                    ), NavOptions.Builder()
                        .build()
                )
            }
        })
        val recyclerView = view.findViewById<RecyclerView>(R.id.list)
        with(recyclerView) {
            adapter = userAdapter
        }

        userListViewModel.usersData.observe(viewLifecycleOwner, Observer { result -> userAdapter.submitList(result) })

        binding.dataLoaded = userListViewModel.dataLoaded
        binding.lifecycleOwner = this
    }

}

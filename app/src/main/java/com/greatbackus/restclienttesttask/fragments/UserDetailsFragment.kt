package com.greatbackus.restclienttesttask.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.greatbackus.restclienttesttask.R
import com.greatbackus.restclienttesttask.databinding.UserDetailsFragmentBinding
import com.greatbackus.restclienttesttask.models.FatUserModel
import com.greatbackus.restclienttesttask.ui.OnUserListItemListener
import com.greatbackus.restclienttesttask.ui.TagListAdapter
import com.greatbackus.restclienttesttask.ui.UserListAdapter
import com.greatbackus.restclienttesttask.viewmodels.UserDetailsViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class UserDetailsFragment : Fragment() {

    private lateinit var viewModel: UserDetailsViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: UserDetailsFragmentBinding

    val args: UserDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        AndroidSupportInjection.inject(this)
        binding = DataBindingUtil.inflate(inflater, R.layout.user_details_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UserDetailsViewModel::class.java)
        viewModel.setUserId(args.userId)
        binding.user = viewModel.userModel
        binding.onEmailClick = object : OnUserListItemListener {
            override fun onClick(item: FatUserModel) {
                val intent = Intent(Intent.ACTION_SENDTO).apply {
                    data = Uri.parse("mailto:")
                    putExtra(Intent.EXTRA_EMAIL, arrayOf(item.userModel.email))
                    putExtra(Intent.EXTRA_TEXT, "")
                    putExtra(Intent.EXTRA_SUBJECT, "")
                }
                if (intent.resolveActivity(activity!!.packageManager) != null) {
                    activity!!.startActivity(intent)
                }

            }
        }
        binding.onPhoneClick = object : OnUserListItemListener {
            override fun onClick(item: FatUserModel) {
                val intent = Intent(Intent.ACTION_DIAL).apply {
                    data = Uri.parse("tel:${item.userModel.phone}")
                }
                if (intent.resolveActivity(activity!!.packageManager)!=null){
                    activity!!.startActivity(intent)
                }
            }
        }
        binding.dataLoaded = viewModel.loaded
        binding.lifecycleOwner = this

        binding.onLocationClick = object : OnUserListItemListener {
            override fun onClick(item: FatUserModel) {
                val intent = Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse("geo:${item.userModel.latitude},${item.userModel.longitude}")
                }
                if (intent.resolveActivity(activity!!.packageManager)!=null) {
                    activity!!.startActivity(intent)
                }
            }
        }

        val userAdapter = UserListAdapter(viewModel.friends, object : OnUserListItemListener {
            override fun onClick(item: FatUserModel) {
                findNavController().navigate(
                    UserDetailsFragmentDirections.actionUserDetailsFragmentSelf(
                        item.userModel.id
                    ), NavOptions.Builder()
                        .build()
                )
            }
        })
        viewModel.friends.observe(viewLifecycleOwner, Observer { userAdapter.submitList(it) })
        val friendsRecyclerView = view.findViewById<RecyclerView>(R.id.list)
        friendsRecyclerView.adapter = userAdapter

        val tagsRecyclerView = view.findViewById<RecyclerView>(R.id.tagList)
        val tagAdapter = TagListAdapter(viewModel.tags)
        tagsRecyclerView.adapter = tagAdapter
        viewModel.tags.observe(viewLifecycleOwner, Observer { tagAdapter.submitList(it) })
    }
}

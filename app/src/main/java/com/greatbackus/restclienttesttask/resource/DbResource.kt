package com.greatbackus.restclienttesttask.resource

abstract class DbResource<T>: Resource<T>() {
    override suspend fun fetch():T = fromDb()
    abstract suspend fun updateCachedData()
}
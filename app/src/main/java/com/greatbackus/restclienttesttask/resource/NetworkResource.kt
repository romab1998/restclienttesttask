package com.greatbackus.restclienttesttask.resource

abstract class NetworkResource<T>: Resource<T>() {

    abstract suspend fun fromNetwork(): T

    abstract suspend fun saveResult(item: T)


    override suspend fun fetch(): T {
        return if (shouldFetch()) {
            val tmp = fromNetwork()
            saveResult(tmp)
            tmp
        } else {
            fromDb()
        }
    }

}
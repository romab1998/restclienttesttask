package com.greatbackus.restclienttesttask.resource

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

abstract class Resource<T> {

    val result = MediatorLiveData<T>()

    init {
        GlobalScope.launch {
            result.postValue(fetch())
        }
    }


    abstract suspend fun fromDb(): T

    abstract suspend fun fetch(): T

    abstract fun shouldFetch(): Boolean

    fun asLiveData() = result as LiveData<T>


}
package com.greatbackus.restclienttesttask.models

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    foreignKeys = [ForeignKey(
        entity = UserModel::class,
        parentColumns = ["id"],
        childColumns = ["userId"],
        onDelete = ForeignKey.CASCADE
    )],
    primaryKeys = ["userId", "tag"]
)
data class TagModel(
    val userId: Long,
    val tag: String
)
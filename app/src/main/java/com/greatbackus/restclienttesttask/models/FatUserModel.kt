package com.greatbackus.restclienttesttask.models

import androidx.room.Embedded
import androidx.room.Relation
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.greatbackus.restclienttesttask.rest.FatUserModelDeserializer

@JsonDeserialize(using = FatUserModelDeserializer::class)
data class FatUserModel(
    @Embedded val userModel: UserModel,
    @Relation(parentColumn = "id", entity = TagModel::class, entityColumn = "userId")
    var tags: List<TagModel>,
    @Relation(parentColumn = "id", entity = FriendModel::class, entityColumn = "userId")
    var friends: List<FriendModel>
)
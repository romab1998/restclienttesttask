package com.greatbackus.restclienttesttask.models

import androidx.room.Entity
import androidx.room.TypeConverters
import com.greatbackus.restclienttesttask.db.UserModelConverter
import java.util.*

@Entity(primaryKeys = ["id"])
@TypeConverters(UserModelConverter::class)
data class UserModel(
    val id: Long,
    val guid: String,
    val isActive: Boolean,
    val eyeColor: EyeColorEnum,
    val balance: Double,
    val age: Int,
    val name: String,
    val gender: GenderEnum,
    val company: String,
    val email: String,
    val phone: String,
    val address: String,
    val about: String,
    val registered: Date,
    val latitude: Double,
    val longitude: Double,
    val favouriteFruit: FavouriteFruitEnum
)



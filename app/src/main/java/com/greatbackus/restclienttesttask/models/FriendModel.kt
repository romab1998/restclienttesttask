package com.greatbackus.restclienttesttask.models

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = UserModel::class,
            parentColumns = ["id"],
            childColumns = ["userId"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    primaryKeys = ["userId", "friendId"]
)
class FriendModel(val userId: Long, val friendId: Long)
package com.greatbackus.restclienttesttask.models

enum class EyeColorEnum(val color: String) {
    BROWN("brown"),
    GREEN("green"),
    BLUE("blue")
}
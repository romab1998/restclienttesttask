package com.greatbackus.restclienttesttask.models

enum class FavouriteFruitEnum(val fruit: String) {
    BANANA("banana"),
    STRAWBERRY("strawberry"),
    APPLE("apple")
}
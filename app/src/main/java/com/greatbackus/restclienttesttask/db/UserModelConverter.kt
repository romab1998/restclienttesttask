package com.greatbackus.restclienttesttask.db

import androidx.room.TypeConverter
import com.greatbackus.restclienttesttask.models.EyeColorEnum
import com.greatbackus.restclienttesttask.models.FavouriteFruitEnum
import com.greatbackus.restclienttesttask.models.GenderEnum
import java.text.SimpleDateFormat
import java.util.*

object UserModelConverter {
    @TypeConverter
    @JvmStatic
    fun eyeColorEnumToString(colorEnum: EyeColorEnum): String = colorEnum.color

    @TypeConverter
    @JvmStatic
    fun stringToEyeColorEnum(color: String): EyeColorEnum = EyeColorEnum.valueOf(color.toUpperCase())

    @TypeConverter
    @JvmStatic
    fun genderEnumToString(genderEnum: GenderEnum) = genderEnum.gender

    @TypeConverter
    @JvmStatic
    fun stringToGenderEnum(gender: String) = GenderEnum.valueOf(gender.toUpperCase())

    @TypeConverter
    @JvmStatic
    fun dateToString(date: Date) = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US).format(date)

    @TypeConverter
    @JvmStatic
    fun stringToDate(string: String) = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US).parse(string)

    @TypeConverter
    @JvmStatic
    fun stringToFavouriteFruitEnum(string: String) = FavouriteFruitEnum.valueOf(string.toUpperCase())

    @TypeConverter
    @JvmStatic
    fun favouriteFruitEnumToString(favouriteFruit: FavouriteFruitEnum) = favouriteFruit.fruit

}
package com.greatbackus.restclienttesttask.db

import androidx.room.*
import com.greatbackus.restclienttesttask.models.FatUserModel
import com.greatbackus.restclienttesttask.models.FriendModel
import com.greatbackus.restclienttesttask.models.TagModel
import com.greatbackus.restclienttesttask.models.UserModel

@Dao
abstract class UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg users: FatUserModel) {
        insert(*users.map { it.userModel }.toTypedArray())
        users.map { it.friends }.forEach { insert(*it.toTypedArray()) }
        users.map { it.tags }.forEach { insert(*it.toTypedArray()) }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(vararg user: UserModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(vararg friendModel: FriendModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(vararg tagModel: TagModel)

    @Transaction
    @Query("SELECT * FROM UserModel WHERE id = :id")
    abstract suspend fun getUserById(id: Long): FatUserModel?

    @Transaction
    @Query("SELECT * FROM UserModel")
    abstract suspend fun getUsers(): List<FatUserModel>

    @Transaction
    @Query("SELECT * FROM UserModel WHERE id in (:ids)")
    abstract suspend fun getUsersByIds(ids: Array<Long>): List<FatUserModel>

}
package com.greatbackus.restclienttesttask.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.greatbackus.restclienttesttask.models.FriendModel
import com.greatbackus.restclienttesttask.models.TagModel
import com.greatbackus.restclienttesttask.models.UserModel

@Database(
    entities = [
        UserModel::class,
        FriendModel::class,
        TagModel::class
    ],
    version = 3
)
abstract class UserDb : RoomDatabase() {
    abstract fun userDao(): UserDao
}
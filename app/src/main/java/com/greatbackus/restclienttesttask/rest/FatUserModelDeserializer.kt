package com.greatbackus.restclienttesttask.rest

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.greatbackus.restclienttesttask.db.UserModelConverter
import com.greatbackus.restclienttesttask.models.*

class FatUserModelDeserializer<T>(vc: Class<T>?): StdDeserializer<FatUserModel>(vc) {

    constructor(): this(null)

    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): FatUserModel {
        val node = p!!.codec.readTree<JsonNode>(p)
        val userModel = UserModel(
            node["id"].longValue(),
            node["guid"].textValue(),
            node["isActive"].booleanValue(),
            EyeColorEnum.valueOf(node["eyeColor"].textValue().toUpperCase()),
            parseDouble(node["balance"].textValue().replace(Regex("[^0-9.]+"), "")),
            node["age"].intValue(),
            node["name"].textValue(),
            GenderEnum.valueOf(node["gender"].textValue().toUpperCase()),
            node["company"].textValue(),
            node["email"].textValue(),
            node["phone"].textValue(),
            node["address"].textValue(),
            node["about"].textValue(),
            UserModelConverter.stringToDate(node["registered"].textValue()),
            node["latitude"].doubleValue(),
            node["longitude"].doubleValue(),
            UserModelConverter.stringToFavouriteFruitEnum(node["favoriteFruit"].textValue().toUpperCase())
        )
        return FatUserModel(
            userModel,
            node["tags"].map { TagModel(userModel.id, it.textValue()) },
            node["friends"].map { FriendModel(userModel.id, it["id"].longValue()) }
        )
    }
}
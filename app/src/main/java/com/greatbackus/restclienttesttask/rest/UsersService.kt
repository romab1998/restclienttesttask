package com.greatbackus.restclienttesttask.rest

import com.greatbackus.restclienttesttask.models.FatUserModel
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface UsersService {
    @GET("s/s8g63b149tnbg8x/users.json")
    fun getUsers(@Query("dl") download: Int=1): Deferred<List<FatUserModel>>
    companion object Factory {
        fun create(): UsersService {
            return Retrofit.Builder()
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(JacksonConverterFactory.create())
                .baseUrl("https://www.dropbox.com/")
                .build().create(UsersService::class.java)
        }
    }
}